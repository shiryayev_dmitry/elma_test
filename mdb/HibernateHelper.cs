﻿using ELMA.DATA;
using NHibernate;
using NHibernate.Cfg;
using System.Web;

namespace mdb
{
    public sealed class NHibernateHelper
    {
        private static ISessionFactory _sessionFactory;
        private static ISessionFactory SessionFactory
        {
            get
            {
                if(_sessionFactory == null)
                {
                    _sessionFactory = new Configuration().
                        Configure().BuildSessionFactory();
                }

                return _sessionFactory;
            }
        }

        public static ISession GetCurrentSession()
        {
            return SessionFactory.OpenSession();
        }
    }

    
}
