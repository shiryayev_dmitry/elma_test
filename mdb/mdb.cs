﻿using ELMA.DATA;
using IELMA;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mdb
{
    public class DataBase : IData
    {
        //Create methods
        #region CreateMethods
        public void CreateVacancion(IVacancy vacancy, string userName)
        {
            var user = GetUserByUserName(userName);
            using (var session = NHibernateHelper.GetCurrentSession())
            {
                using (var tr = session.BeginTransaction())
                {
                    Vacancy vc = (Vacancy)vacancy;
                    vc.User = new User() { UserID = user.UserID };
                    vc.IsActualy = true;
                    session.Save(vc);
                    tr.Commit();
                }
            }
        }

        public void CreateWorkExperience(IEnumerable<IWorkExperience> workExperiences, int candidate_id)
        {
            var candidate = GetCandidateById(candidate_id);
            foreach (WorkExperience experience in workExperiences)
            {
                using (var session = NHibernateHelper.GetCurrentSession())
                {
                    using (var tr = session.BeginTransaction())
                    {
                        experience.Candidate = (Candidate)candidate;
                        session.SaveOrUpdate(experience);
                        tr.Commit();
                    }
                }
            }

        }

        public int CreateCandidate(ICandidate _candidate, string userName)
        {
            var user = GetUserByUserName(userName);
            using (var session = NHibernateHelper.GetCurrentSession())
            {
                using (var tr = session.BeginTransaction())
                {
                    Candidate candidate = (Candidate)_candidate;
                    candidate.User = new User() { UserID = user.UserID };
                    candidate.IsActualy = true;
                    int candidateId = (int)session.Save(candidate);
                    tr.Commit();
                    return candidateId;
                }
            }
        }

        public void DoUpdateActualyVacancy(int vacancy_id)
        {
            using (var session = NHibernateHelper.GetCurrentSession())
            {
                using (var tr = session.BeginTransaction())
                {
                    Vacancy vacancy = (Vacancy)GetVacancyById(vacancy_id);
                    vacancy.User = new User() { UserID = vacancy.UserID };
                    vacancy.IsActualy = !vacancy.IsActualy;
                    session.SaveOrUpdate(vacancy);
                    tr.Commit();
                }
            }
        }

        public void DoUpdateActualyCandidate(int candidate_id)
        {
            using (var session = NHibernateHelper.GetCurrentSession())
            {
                using (var tr = session.BeginTransaction())
                {
                    Candidate candidate = (Candidate)GetCandidateById(candidate_id);
                    candidate.User = new User() { UserID = candidate.UserID };
                    candidate.IsActualy = !candidate.IsActualy;
                    session.SaveOrUpdate(candidate);
                    tr.Commit();
                }
            }
        }
        #endregion

        #region ReadMethods
        public IUser GetUserById(int userId)
        {
            using (var session = NHibernateHelper.GetCurrentSession())
            {
                return session.Query<User>().FirstOrDefault(t => t.UserID == userId);
            }
        }

        public IUser GetUserAuth(string login, string password)
        {
            using (var session = NHibernateHelper.GetCurrentSession())
            {
                return session.Query<User>().FirstOrDefault(t => t.UserName == login && t.Password == password);
            }
        }

        public IVacancy GetVacancyById(int vacancionId)
        {
            using (var session = NHibernateHelper.GetCurrentSession())
            {
                return session.Query<Vacancy>().FirstOrDefault(t => t.VacancyID == vacancionId);
            }
        }

        public ICandidate GetCandidateById(int candidateId)
        {
            using (var session = NHibernateHelper.GetCurrentSession())
            {
                return session.Query<Candidate>().FirstOrDefault(t => t.CandidateID == candidateId);
            }
        }
        public IEnumerable<IWorkExperience> GetWorkExperiencesByCandidateId(int candidate_id)
        {
            using (var session = NHibernateHelper.GetCurrentSession())
            {
                return session.Query<WorkExperience>().
                    Where(t => t.Candidate.CandidateID == candidate_id).ToList();
            }
        }
        public IEnumerable<IRole> GetAllRoles()
        {
            using (var session = NHibernateHelper.GetCurrentSession())
            {
                return session.Query<Role>().ToList();
            }
        }

        public IEnumerable<IUser> GetAllUsers()
        {
            using (var session = NHibernateHelper.GetCurrentSession())
            {
                return session.Query<User>().ToList();
            }
        }

        public IEnumerable<IVacancy> GetAllVacancions()
        {
            using (var session = NHibernateHelper.GetCurrentSession())
            {
                return session.Query<Vacancy>().ToList();
            }
        }

        public IEnumerable<IVacancy> GetVacancionsByUserId(int userId)
        {
            using (var session = NHibernateHelper.GetCurrentSession())
            {
                return session.Query<Vacancy>().Where(
                    t => t.User.UserID == userId).ToList();
            }
        }

        public IEnumerable<ICandidate> GetCandidatesByUserID(int userId)
        {
            using (var session = NHibernateHelper.GetCurrentSession())
            {
                return session.Query<Candidate>().Where(t => t.User.UserID == userId).ToList();
            }
        }

        public IEnumerable<ICandidate> GetAllCandidates()
        {
            using (var session = NHibernateHelper.GetCurrentSession())
            {
                return session.Query<Candidate>().ToList();
            }
        }

        public string[] GetRolesByUserName(string userName)
        {
            var user = GetUserByUserName(userName);

            if (user != null)
            {
                using (var session = NHibernateHelper.GetCurrentSession())
                {
                    var roles = GetAllRoles().FirstOrDefault(t => t.Id == user.Role.Id);

                    if (roles != null)
                    {
                        return new string[] { roles.Name };
                    }
                }
            }

            return new string[] { };
        }

        public bool CheckRoleForUser(string userName, string roleName)
        {
            var user = GetUserByUserName(userName);

            if (user != null)
            {
                using (var session = NHibernateHelper.GetCurrentSession())
                {
                    var role = GetAllRoles().FirstOrDefault(t => t.Id == user.Role.Id);
                    if (role != null && roleName == role.Name)
                        return true;
                }
            }

            return false;

        }

        private User GetUserByUserName(string userName)
        {
            using (var session = NHibernateHelper.GetCurrentSession())
            {
                return session.Query<User>().FirstOrDefault(t => t.UserName == userName);
            }
        }
        #endregion

    }
}
