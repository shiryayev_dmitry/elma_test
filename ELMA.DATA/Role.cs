﻿using IELMA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELMA.DATA
{
    public class Role : IRole
    {
        private ICollection<User> _users;
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual ICollection<User> Users
        {
            get
            {
                return _users;
            }
            set
            {
                this._users = value;
            }
        }

        public virtual void Add(User user)
        {
            if(_users == null)
            {
                _users = new List<User>();
            }
            user.Role = this;
            _users.Add(user); 
        }
    }
}
