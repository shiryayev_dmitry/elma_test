﻿using IELMA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELMA.DATA
{
    public class User : IUser
    {
        private ICollection<Vacancy> _vacancies;
        private ICollection<Candidate> _candidates;
        public virtual int UserID { get; set; }
        public virtual Role Role { get; set; }
        public virtual string UserName { get; set; }
        public virtual string Password { get; set; }
        public virtual ICollection<Candidate> Candidates
        {
            get
            {
                return _candidates;
            }
            set
            {
                _candidates = value;
            }
        }
        public virtual ICollection<Vacancy> Vacancies
        {
            get
            {
                return _vacancies;
            }
            set
            {
                this._vacancies = value;
            }
        }
        public virtual void Add(Vacancy vacancy)
        {
            if (_vacancies == null)
            {
                _vacancies = new List<Vacancy>();
            }
            vacancy.User = this;
            _vacancies.Add(vacancy);
        }

        public virtual void Add(Candidate candidate)
        {
            if (_candidates == null)
            {
                this._candidates = new List<Candidate>();
            }
            candidate.User = this;
            _candidates.Add(candidate);
        }
    }

    public class Login : ILogin
    {
        public string Password { get; set; }
        public string UserName { get; set; }
    }
}
