﻿using IELMA;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELMA.DATA
{
    public class Candidate : ICandidate
    {
        private ICollection<WorkExperience> _workExperiences;
        public virtual int CandidateID { get; set; }
        [Required]
        [Display(Name = "Имя")]
        public virtual string FirstName { get; set; }
        [Required]
        [Display(Name = "Отчество")]
        public virtual string MiddleName { get; set; }
        
        [Display(Name = "Фамилия")]
        public virtual string LastName { get; set; }
        [Required]
        [Display(Name = "Дата рождения")]
        public virtual DateTime Birthday { get; set; }
        [Required]
        [Display(Name = "Ожидаемая зарплата")]
        public virtual decimal Salary { get; set; }
        [Required]
        [Display(Name = "Позиция")]
        public virtual string Position { get; set; }
        public virtual byte[] ImageData { get; set; }
        public virtual string ImageMimeType { get; set; }
        public virtual bool IsActualy { get; set; }
        public virtual User User { get; set; }
        public virtual int UserID
        {
            get
            {
                return User != null ? User.UserID : 0;
            }
            set
            {
                UserID = value;
            }
        }
        public virtual ICollection<WorkExperience> WorkExperiences
        {
            get
            {
                return _workExperiences;
            }
            set
            {
                _workExperiences = value;
            }
        }
        public virtual void Add(WorkExperience experience)
        {
            if(_workExperiences == null)
            {
                _workExperiences = new List<WorkExperience>();
            }

            experience.Candidate = this;
            _workExperiences.Add(experience);
        }
    }
}
