﻿using IELMA;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELMA.DATA
{
    public class Vacancy : IVacancy
    {
        public virtual int VacancyID { get; set; }
        public virtual User User { get; set; }
        [Required]
        [Display(Name = "Описание")]
        public virtual string Description { get; set; }
        [Required]
        [Display(Name = "Название компании")]
        public virtual string CompanyName { get; set; }
        [Required]
        [Display(Name = "Ожидаемая зарплата")]
        public virtual decimal Salary { get; set; }
        [Required]
        [Display(Name = "Обязанности")]
        public virtual string Requirement { get; set; }
        [Required]
        [Display(Name = "Позиция")]
        public virtual string Position { get; set; }
        public virtual bool IsActualy { get; set; }
        public virtual int UserID
        {
            get
            {
                return User !=null ? User.UserID:0;
            }
            set
            {
                UserID = value;
            }
        }
        
    }
}
