﻿using IELMA;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELMA.DATA
{
    public class WorkExperience : IWorkExperience
    {
        public virtual int ExperienceID { get; set; }
        public virtual Candidate Candidate { get; set; }
        [Required]
        [Display(Name = "Название компании")]
        public virtual string CompanyName { get; set; }
        [Required]
        [Display(Name = "Позиция")]
        public virtual string Position { get; set; }
        [Required]
        [Display(Name = "Описание")]
        public virtual string Description { get; set; }
    }
}
