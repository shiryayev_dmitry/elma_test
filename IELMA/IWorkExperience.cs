﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IELMA
{
    public interface IWorkExperience
    {
        int ExperienceID { get; set; }
        string CompanyName { get; set; }
        string Position { get; set; }
        string Description { get; set; }
    }
}
