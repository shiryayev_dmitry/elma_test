﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace IELMA
{
    interface IAutentification
    {
        /// <summary>
        /// Конекст (тут мы получаем доступ к запросу и кукисам)
        /// </summary>
        HttpContext HttpContext { get; set; }
        IUser Login(string login, string password, bool isPersistent);
        IUser Login(string login);
        void LogOut();
        IPrincipal CurrentUser { get; }
    }
}
