﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IELMA
{
    public interface IVacancy : IUserID
    {
        int VacancyID { get; set; }
        string Description { get; set; }
        string CompanyName { get; set; }
        bool IsActualy { get; set; }
        decimal Salary { get; set; }
        string Requirement { get; set; }
        string Position { get; set; }
    }
}
