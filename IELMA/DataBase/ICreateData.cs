﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IELMA
{
    public interface ICreateData
    {
        /// <summary>
        /// Создать вакансию
        /// </summary>
        /// <param name="vacancy"></param>
        /// <param name="user_id"></param>
        void CreateVacancion(IVacancy vacancy, string userName);
        /// <summary>
        /// Добавить опыт
        /// </summary>
        /// <param name="workExperiences"></param>
        /// <param name="candidateId"></param>
        void CreateWorkExperience(IEnumerable<IWorkExperience> workExperiences, int candidateId);
        /// <summary>
        /// Создать резюме
        /// </summary>
        /// <param name="_candidate"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        int CreateCandidate(ICandidate _candidate, string userName);
        /// <summary>
        /// Обновить актуальность вакансии
        /// </summary>
        /// <param name="vacancyId"></param>
        void DoUpdateActualyVacancy(int vacancyId);
        /// <summary>
        /// Обновить актуальность резюме
        /// </summary>
        /// <param name="candidateId"></param>
        void DoUpdateActualyCandidate(int candidateId);
    }
}
