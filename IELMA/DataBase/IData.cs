﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IELMA
{
    public interface IData : IReadData, ICreateData
    {

    }

    public interface IReadData
    {
        /// <summary>
        /// Получить пользователя по ID
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IUser GetUserById(int userId);
        /// <summary>
        /// Получить данные пользователя после авторизации
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        IUser GetUserAuth(string login, string password);
        /// <summary>
        /// Получить вакансию по её id
        /// </summary>
        /// <param name="vacancionId"></param>
        /// <returns></returns>
        IVacancy GetVacancyById(int vacancionId);
        /// <summary>
        /// Получить кандидата по ID
        /// </summary>
        /// <param name="candidateId"></param>
        /// <returns></returns>
        ICandidate GetCandidateById(int candidateId);
        /// <summary>
        /// Получить список прошлых мест работы, указанных в резюме
        /// </summary>
        /// <param name="candidateId"></param>
        /// <returns></returns>
        IEnumerable<IWorkExperience> GetWorkExperiencesByCandidateId(int candidateId);
        /// <summary>
        /// Получить список ролей
        /// </summary>
        /// <returns></returns>
        IEnumerable<IRole> GetAllRoles();
        /// <summary>
        /// Получить список пользователей
        /// </summary>
        /// <returns></returns>
        IEnumerable<IUser> GetAllUsers();
        /// <summary>
        /// Получить список всех вакансий
        /// </summary>
        /// <returns></returns>
        IEnumerable<IVacancy> GetAllVacancions();
        /// <summary>
        /// Получить список вакансий по пользователю
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IEnumerable<IVacancy> GetVacancionsByUserId(int userId);
        /// <summary>
        /// Получить список резюме у пользователя
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IEnumerable<ICandidate> GetCandidatesByUserID(int userId);
        /// <summary>
        /// Получить список всех кандидатов
        /// </summary>
        /// <returns></returns>
        IEnumerable<ICandidate> GetAllCandidates();
        
        /// <summary>
        /// Получить список ролей
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        string[] GetRolesByUserName(string userName);
        /// <summary>
        /// Проверить соответствует ли роль пользователя
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="roleName"></param>
        /// <returns></returns>
        bool CheckRoleForUser(string userName, string roleName);
    }
}
