﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IELMA
{
    public interface ICandidate:IUserID
    {
        int CandidateID { get; set; }
        string FirstName { get; set; }
        string MiddleName { get; set; }
        string LastName { get; set; }
        DateTime Birthday { get; set; }
        decimal Salary { get; set; }
        string Position { get; set; }
        byte[] ImageData { get; set; }
        string ImageMimeType { get; set; }
        bool IsActualy { get; set; }
    }
}
