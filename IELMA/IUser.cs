﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IELMA
{
    public interface IUserID
    {
        int UserID { get; set; }
    }

    public interface IUser : IUserID
    {
        string UserName { get; set; }
        string Password { get; set; }
    }

    public interface ILogin
    {
        string UserName { get; set; }
        string Password { get; set; }
    }
}
