﻿using ELMA.DATA;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ELMA.Models
{
    public static class Filter
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public static void SetVacancyFilter(ref VacancyViewModel vvm)
        {
            var positionList = new List<string>() { "Все" };
            var companyList = new List<string>() { "Все" };
            logger.Info("Set vacancy filter");
            foreach (var position in vvm.Vacancies.Select(t => t.Position))
            {
                if (!positionList.Contains(position))
                    positionList.Add(position);
            }

            foreach(var company in vvm.Vacancies.Select(t=> t.CompanyName))
            {
                if (!companyList.Contains(company))
                    companyList.Add(company);
            }

            vvm.Positions = new SelectList(positionList);
            vvm.Companies = new SelectList(companyList);
        }
        public static void SetCandidateFilter(ref CandidatesViewModel cvm)
        {
            var positionList = new List<string>() { "Все" };
            logger.Info("Set candidate filter");
            foreach (var position in cvm.Candidates.Select(t => t.Position))
            {
                if (!positionList.Contains(position))
                    positionList.Add(position);
            }

            cvm.Positions = new SelectList(positionList);
        }

        public static void DoFilteredVacancy (ref VacancyViewModel vvm, VacancyFilter vf)
        {
            vvm.vacancyFilter = vf;

            if (vf.Position != null)
                vvm.Vacancies = vvm.Vacancies.
                    Where(t => t.Position == vf.Position || vf.Position == "Все");

            if (vf.CompanyName != null)
                vvm.Vacancies = vvm.Vacancies.
                    Where(t => t.CompanyName == vf.CompanyName || vf.CompanyName == "Все");

            if (vf.Salary > 0)
                vvm.Vacancies = vvm.Vacancies.Where(t => t.Salary >= vf.Salary).
                    OrderBy(t => t.Salary);            
        }

        public static void DoFilteredCandidate(ref CandidatesViewModel cvm, CandidateFilter cf)
        {
            cvm.candidateFilter = cf;

            if (cf.Position != null)
                cvm.Candidates = cvm.Candidates.
                    Where(t => t.Position == cf.Position || cf.Position == "Все");

            if (cf.Salary > 0)
                cvm.Candidates = cvm.Candidates.Where(t => t.Salary >= cf.Salary).
                    OrderBy(t => t.Salary);
        }

        public static void ChangeCandidateExperience(ref CandidateModel candidateModel,
                                    string addExperience,
                                    string deleteExperience)
        {
            if (addExperience != null)
            {
                logger.Info("Add new experience");
                if (candidateModel.WorkExperiences == null)
                    candidateModel.WorkExperiences = new List<WorkExperience>();

                candidateModel.WorkExperiences.Add(new WorkExperience());
            }

            if (deleteExperience != null)
            {
                logger.Info("Delete last experience");
                candidateModel.WorkExperiences.RemoveAt(candidateModel.WorkExperiences.Count() - 1);
            }
        }
    }
}