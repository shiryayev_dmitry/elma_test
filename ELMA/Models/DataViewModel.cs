﻿using ELMA.DATA;
using IELMA;
using mdb;
using mock;

namespace ELMA.Models
{
    public static class DataModel
    {
        static DataModel()
        {
            Repository = new DataBase();
            //Repository = new Mock();
        }

        public static IData Repository;
    }
}