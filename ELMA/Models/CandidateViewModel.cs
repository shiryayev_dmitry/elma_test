﻿using ELMA.DATA;
using IELMA;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ELMA.Models
{
    public class CandidatesViewModel
    {
        public IEnumerable<ICandidate> Candidates { get; set; }
        public SelectList Positions { get; set; }
        public CandidateFilter candidateFilter { get; set; }
    }

    public class CandidateModel
    {
        public Candidate Candidate { get; set; }
        public List<WorkExperience> WorkExperiences { get; set; }
    }

    public class CandidateFilter : Candidate { }
}