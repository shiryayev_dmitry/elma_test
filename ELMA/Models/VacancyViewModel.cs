﻿using ELMA.DATA;
using IELMA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ELMA.Models
{
    public class VacancyViewModel
    {
        public IEnumerable<IVacancy> Vacancies { get; set; }
        public SelectList Positions { get; set; }
        public SelectList Companies { get; set; }
        public VacancyFilter vacancyFilter { get; set; }
    }

    public class VacancyFilter:Vacancy { }
}