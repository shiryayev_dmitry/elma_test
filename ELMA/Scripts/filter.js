﻿function ToggleFilter() {
    $('#filter').slideToggle(300, function () {
        if ($(this).is(':hidden')) {
            $('#filter_toggle').html("Фильтр");
        } else {
            $("#filter_toggle").html("Скрыть фильтр");
        }
    });
}