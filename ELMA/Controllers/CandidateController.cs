﻿using ELMA.DATA;
using ELMA.Models;
using ELMA.Models.Atributes;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ELMA.Controllers
{
    public class CandidateController : Controller
    {
        public static Logger logger = LogManager.GetCurrentClassLogger();
        public ActionResult Candidate()
        {
            return View();
        }

        [HttpGet]
        [Authorize]
        public ActionResult Candidate(int candidateId)
        {
            logger.Info("Get candidate params");
            var candidate = (Candidate)DataModel.Repository.GetCandidateById(candidateId);
            var user = (User)DataModel.Repository.GetUserById(candidate.UserID);
            candidate.User = user;
            var workExperience = DataModel.Repository.GetWorkExperiencesByCandidateId(candidateId);
            var model = new CandidateModel()
            {
                Candidate = candidate,
                WorkExperiences = (List<WorkExperience>)workExperience
            };
            logger.Info("Open candidate view");
            return View(model);
        }

        [Authorize]
        public ActionResult Create(CandidateModel candidateModel,
                                    string addExperience,
                                    string deleteExperience,
                                    string createCandidate,
                                    HttpPostedFileBase image = null)
        {
            logger.Info("Start create new candidate");
            Models.Filter.ChangeCandidateExperience(ref candidateModel, addExperience, deleteExperience);

            if (createCandidate != null && ModelState.IsValid)
            {
                logger.Info("Create candidate if all params is Valid");
                if (image != null)
                {
                    logger.Info("Add image");
                    candidateModel.Candidate.ImageMimeType = image.ContentType;
                    candidateModel.Candidate.ImageData = new byte[image.ContentLength];
                    image.InputStream.Read(candidateModel.Candidate.ImageData, 0, image.ContentLength);
                    logger.Info("Create Candidate");
                    int candidate_id = DataModel.Repository.CreateCandidate(candidateModel.Candidate,  User.Identity.Name);
                    logger.Info("Create work experience for candidate");
                    DataModel.Repository.CreateWorkExperience(candidateModel.WorkExperiences, candidate_id);
                }
                return RedirectToRoute(new { controller = "Home", action = "Index" });
            }
                

            return View(candidateModel);
        }

        [Authorize]
        public FileContentResult GetImage(int candidate_id)
        {
            logger.Info("Get image");
            logger.Info("Get candidate by id");
            var candidate = (Candidate)DataModel.Repository.GetCandidateById(candidate_id);
            try
            {
                if (candidate != null)
                {
                    logger.Info("Return image file");
                    var ss = candidate.ImageData.ToString();
                    return File(candidate.ImageData, candidate.ImageMimeType);
                }
                else
                {
                    logger.Info("Candidate not found");
                    return null;
                }
            }
            catch(Exception ex)
            {
                logger.Error(ex.Message);
                return null;
            }
            
        }
        [Authorize]
        public ActionResult Close(int candidateId)
        {
            logger.Info("Close candidate");
            DataModel.Repository.DoUpdateActualyCandidate(candidateId);
            return RedirectToRoute(new { controller = "Home", action = "Index" });
        }
    }
}