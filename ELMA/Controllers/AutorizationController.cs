﻿using ELMA.DATA;
using NLog;
using System.Web.Mvc;
using System.Web.Security;

namespace ELMA.Controllers
{
    public class AutorizationController : Controller
    {
        public static Logger logger = LogManager.GetCurrentClassLogger();
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Login model)
        {
            logger.Info("Start autorization");
            var user = Models.DataModel.Repository.GetUserAuth(model.UserName, model.Password);
            if(user != null)
            {
                logger.Info("Autorization");
                FormsAuthentication.SetAuthCookie(user.UserName, true);
                return RedirectToRoute(new { controller = "Home", action = "Index" });
            }
            logger.Info("Not autorization");
            return View(model);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SetAuthCookie(null, false);
            return RedirectToAction("Login");
        }
    }
}