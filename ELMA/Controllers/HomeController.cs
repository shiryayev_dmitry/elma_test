﻿using ELMA.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Filter = ELMA.Models.Filter;

namespace ELMA.Controllers
{
    public class HomeController : Controller
    {
        public static Logger logger = LogManager.GetCurrentClassLogger();

        [Authorize]
        public ActionResult Index()
        {
            logger.Info("Open start web page");

            if (User.IsInRole("Admin"))
            {
                logger.Info("redirect to admin view");
                return RedirectToAction("Admin");
            }
                

            if (User.IsInRole("Jobseeker"))
            {
                logger.Info("redirect to Candidates view");
                return RedirectToAction("Candidates");
            }
                

            if (User.IsInRole("Employee"))
            {
                logger.Info("redirect to Vacancions view");
                return RedirectToAction("Vacancions");
            }

            logger.Info("Role not found. Redirect to Login view");
            return RedirectToRoute(new { controller = "Autorization", action="Login"});
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Admin()
        {
            logger.Info("Show admin page");
            var model = DataModel.Repository.GetAllUsers();
            return View(model);
        }

        [Authorize]
        public ActionResult Vacancions(VacancyFilter vacancyFilter, bool userVacancy = false)
        {
            logger.Info("Get vacancies data");
            var vacancyModel = new VacancyViewModel();
            vacancyModel.Vacancies = DataModel.Repository.GetAllVacancions();

            if(userVacancy)
            {
                logger.Info("Show users vacancion");
                var user = DataModel.Repository.GetAllUsers().FirstOrDefault(t=> t.UserName == User.Identity.Name);
                vacancyModel.Vacancies = vacancyModel.Vacancies.Where(t => t.UserID == user.UserID);
            }
            else
            {
                logger.Info("Show all Actualy vacancions");
                vacancyModel.Vacancies = vacancyModel.Vacancies.Where(t => t.IsActualy || User.IsInRole("Admin"));
            }

            logger.Info("Set filter data and filtered vacancies");
            Filter.SetVacancyFilter(ref vacancyModel);
            Filter.DoFilteredVacancy(ref vacancyModel, vacancyFilter);

            logger.Info("Show employee page");
            return View(vacancyModel);
        }

        [Authorize]
        public ActionResult Candidates(CandidateFilter candidateFilter, bool userVacancy = false)
        {
            logger.Info("Get candidates data");
            var candidateModel = new CandidatesViewModel();
            candidateModel.Candidates = DataModel.Repository.GetAllCandidates();

            if (userVacancy)
            {
                logger.Info("Show users candidates");
                var user = DataModel.Repository.GetAllUsers().FirstOrDefault(t => t.UserName == User.Identity.Name);
                candidateModel.Candidates = candidateModel.Candidates.Where(t => t.UserID == user.UserID);
            }
            else
            {
                logger.Info("Show all actialy candidates");
                candidateModel.Candidates = candidateModel.Candidates.Where(t => t.IsActualy || User.IsInRole("Admin"));
            }

            logger.Info("Set filter data and filtered candidates");
            Filter.SetCandidateFilter(ref candidateModel);
            Filter.DoFilteredCandidate(ref candidateModel, candidateFilter);

            logger.Info("Show jobseeker page");
            return View(candidateModel);
        }
    }
}