﻿using ELMA.DATA;
using ELMA.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ELMA.Controllers
{
    public class VacancyController : Controller
    {
        public static Logger logger = LogManager.GetCurrentClassLogger();
        [Authorize]
        public ActionResult Vacancy()
        {
            return View();
        }

        [HttpGet]
        [Authorize]
        public ActionResult Vacancy(int vacancyId)
        {
            logger.Info("Get params for vacancy");
            var model = (Vacancy)DataModel.Repository.GetVacancyById(vacancyId);
            var user = DataModel.Repository.GetUserById(model.UserID);
            model.User = (User)user;
            logger.Info("Show vacancy by id");
            return View(model);
        }

        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        [Authorize]
        public ActionResult CreateVacancy(Vacancy vc)
        {
            logger.Info("Create vacancy");
            if(ModelState.IsValid)
            {
                DataModel.Repository.CreateVacancion(vc, User.Identity.Name);
                return RedirectToRoute(new { controller = "Home", action = "Candidates" });
            }
            return RedirectToAction("Create");
        }

        [Authorize]
        public ActionResult Close(int vacancyId)
        {
            logger.Info("Close vacancy");
            DataModel.Repository.DoUpdateActualyVacancy(vacancyId);
            return RedirectToRoute(new { controller = "Home", action = "Index" });
        }
    }
}