USE [ELMA]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Candidates](
	[candidate_id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[firstname] [varchar](255) NOT NULL,
	[lastname] [varchar](255) NOT NULL,
	[middlename] [varchar](255) NOT NULL,
	[birthdate] [datetime] NOT NULL,
	[salary] [decimal](18, 0) NOT NULL,
	[position] [varchar](255) NOT NULL,
	[imageData] [varbinary](max) NOT NULL,
	[imageMimeType] [varchar](255) NOT NULL,
	[isActualy] [bit] NOT NULL,
 CONSTRAINT [PK_CANDIDATE] PRIMARY KEY CLUSTERED 
(
	[candidate_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Candidates] ADD  DEFAULT ('true') FOR [isActualy]
GO

CREATE TABLE [dbo].[Roles](
	[rolesid] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NOT NULL,
 CONSTRAINT [PK_ROLES] PRIMARY KEY CLUSTERED 
(
	[rolesid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Users](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NOT NULL,
	[UserName] [varchar](255) NOT NULL,
	[Password] [varchar](255) NOT NULL,
 CONSTRAINT [PK_USERS] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [users_fk0] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([rolesid])
ON UPDATE CASCADE
GO

ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [users_fk0]
GO

CREATE TABLE [dbo].[Vacancies](
	[vacancy_id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[description] [varchar](255) NOT NULL,
	[company_name] [varchar](255) NOT NULL,
	[salary] [decimal](18, 0) NOT NULL,
	[position] [varchar](255) NOT NULL,
	[requirement] [varchar](255) NOT NULL,
	[isActualy] [bit] NOT NULL,
 CONSTRAINT [PK_VACANCIES] PRIMARY KEY CLUSTERED 
(
	[vacancy_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Vacancies] ADD  DEFAULT ('true') FOR [isActualy]
GO

ALTER TABLE [dbo].[Vacancies]  WITH CHECK ADD  CONSTRAINT [Vacancies_fk0] FOREIGN KEY([user_id])
REFERENCES [dbo].[Users] ([UserID])
ON UPDATE CASCADE
GO

ALTER TABLE [dbo].[Vacancies] CHECK CONSTRAINT [Vacancies_fk0]
GO

CREATE TABLE [dbo].[WorkExperience](
	[experience_id] [int] IDENTITY(1,1) NOT NULL,
	[candidate_id] [int] NOT NULL,
	[company_name] [varchar](255) NOT NULL,
	[position] [varchar](255) NOT NULL,
	[description] [varchar](255) NOT NULL,
 CONSTRAINT [PK_WORKEXPERIENCE] PRIMARY KEY CLUSTERED 
(
	[experience_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[WorkExperience]  WITH CHECK ADD  CONSTRAINT [workExperience_fk0] FOREIGN KEY([candidate_id])
REFERENCES [dbo].[Candidates] ([candidate_id])
ON UPDATE CASCADE
GO

ALTER TABLE [dbo].[WorkExperience] CHECK CONSTRAINT [workExperience_fk0]
GO


