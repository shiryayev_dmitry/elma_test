﻿using ELMA.DATA;
using System;
using System.Collections.Generic;

namespace mock
{
    internal static class TestProps
    {
        public static IEnumerable<Role> Roles = new List<Role>()
        {
            new Role { Id = 1, Name = "Employee"},
            new Role { Id = 2, Name = "Jobseeker"},
            new Role { Id = 3, Name = "Admin"}
        };

        public static IEnumerable<User> Users = new List<User>()
        {
            new User {UserName="Admin@mail.ru", Role=new Role(){ Id = 3}, UserID=0},
            new User {UserName="Dmitry1@mail.ru", Role=new Role(){ Id = 1}, UserID=1},
            new User {UserName="Dmitry2@mail.ru", Role=new Role(){ Id = 1}, UserID=2},
            new User {UserName="Dmitry2@mail.ru", Role=new Role(){ Id = 1}, UserID=3},
            new User {UserName="Employee1@mail.ru", Role=new Role(){ Id = 2}, UserID=4},
            new User {UserName="Employee2@mail.ru", Role=new Role(){ Id = 2}, UserID=5},
        };

        public static IEnumerable<Candidate> Candidates = new List<Candidate>()
        {
            new Candidate { CandidateID=1, FirstName="Василий", LastName="Пупкин", MiddleName="Игоревич",
                Birthday=DateTime.Now, User = new User(){ UserID = 1}, Position=".NET программист", Salary=30000},
            new Candidate { CandidateID=2, FirstName="Анатолий", LastName="Попкин", MiddleName="Игоревич",
                Birthday=DateTime.Now, User = new User(){ UserID = 2}, Position="PHP программист", Salary=30000},
            new Candidate { CandidateID=2, FirstName="Георгий", LastName="Сюткин", MiddleName="Игоревич",
                Birthday=DateTime.Now, User = new User(){ UserID = 3}, Position="PHP программист", Salary=30000}
        };

        public static IEnumerable<Vacancy> Vacancies = new List<Vacancy>()
        {
            new Vacancy { CompanyName="ELMA", Position=".Net программист", Salary = 30000,
                User = new User(){ UserID = 4}, VacancyID=1},
            new Vacancy { CompanyName="РИТ", Position="PHP программист", Salary = 30000,
                 User = new User(){ UserID = 5}, VacancyID=2}
        };

        public static IEnumerable<WorkExperience> WorkExperiences = new List<WorkExperience>()
        {
            new WorkExperience { CompanyName="ELMA", Description="Работа в ELMA",
                ExperienceID =1, Position=".Net", Candidate=new Candidate(){ CandidateID=1 } },
            new WorkExperience { CompanyName="РИТ", Description="Работа в ELMA",
                ExperienceID =1, Position=".Net", Candidate=new Candidate(){ CandidateID=1 } }
        };
    }
}
