﻿using ELMA.DATA;
using IELMA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mock
{
    public class Mock : IData
    {
        #region ChangeMethods
        public void CreateVacancion(IVacancy vacancy, string userName)
        {
            
        }
        public void CreateWorkExperience(IEnumerable<IWorkExperience> workExperiences, int candidate_id)
        {
        }
        public int CreateCandidate(ICandidate _candidate, string userName)
        {
            return 0;
        }
        public void DoUpdateActualyVacancy(int vacancy_id)
        {
        }
        public void DoUpdateActualyCandidate(int candidate_id)
        {
        }
        #endregion

        #region ReadData
        public IUser GetUserById(int userId)
        {
            return TestProps.Users.FirstOrDefault(t => t.UserID == userId);
        }
        public IUser GetUserAuth(string login, string password)
        {
            return TestProps.Users.FirstOrDefault(t => t.UserName == login);
        }
        public IVacancy GetVacancyById(int vacancionId)
        {
            return TestProps.Vacancies.FirstOrDefault(t => t.VacancyID == vacancionId);
        }
        public ICandidate GetCandidateById(int candidateId)
        {
            return TestProps.Candidates.FirstOrDefault(t => t.CandidateID == candidateId);
        }

        public IEnumerable<IWorkExperience> GetWorkExperiencesByCandidateId(int candidateId)
        {
            return TestProps.WorkExperiences.Where(t => t.Candidate.CandidateID == candidateId);
        }
        public IEnumerable<IRole> GetAllRoles()
        {
            return TestProps.Roles;
        }
        public IEnumerable<IUser> GetAllUsers()
        {
            return TestProps.Users;
        }
        public IEnumerable<IVacancy> GetAllVacancions()
        {
            return TestProps.Vacancies;
        }
        public IEnumerable<IVacancy> GetVacancionsByUserId(int userId)
        {
            return TestProps.Vacancies.Where(t => t.User.UserID == userId);
        }
        public IEnumerable<ICandidate> GetCandidatesByUserID(int userID)
        {
            return TestProps.Candidates.Where(t => t.User.UserID == userID);
        }
        public IEnumerable<ICandidate> GetAllCandidates()
        {
            return TestProps.Candidates;
        }
        public string[] GetRolesByUserName(string userName)
        {
            var user = TestProps.Users.FirstOrDefault(t => t.UserName == userName);
            if (user != null)
            {
                var roles = TestProps.Roles.FirstOrDefault(t => t.Id == user.Role.Id);

                if (roles != null)
                {
                    return new string[] { roles.Name };
                }
            }

            return new string[] { };
        }
        public bool CheckRoleForUser(string userName, string roleName)
        {
            var user = TestProps.Users.FirstOrDefault(t => t.UserName == userName);
            if (user != null)
            {
                var role = TestProps.Roles.FirstOrDefault(t => t.Id == user.Role.Id);
                if (role != null && roleName == role.Name)
                    return true;
            }

            return false;
        }
        private User GetUserByUserName(string userName)
        {
            return TestProps.Users.FirstOrDefault(t => t.UserName == userName);
        }
        #endregion

        public void CreateUser()
        {
        }
    }
}
