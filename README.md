# README #
тестовое задание, подготовленное для компании ELMA
Затрачено времени: 
проектирование-3 часа; разработка frontend - 5 часов; разработка backend - 20 часов;
изучение библиотеки NHibernate - 5 часов;
Итого: 33 часа

### Как подключить бд ###
Для подключения к бд необходимо перейти в модуль mdb и изменить значение 
<property name="connection.connection_string">
	Data Source=LAPTOP-QQHK44FB;Initial Catalog=ELMA;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False
</property>

в файле hibernate.cfg.xml

### Реализация БД ###
Для создания таблиц, используемых в приложении воспользуйтесь кодом из файла
'MsSQL_Tables.txt' или 'SQLCreateTable.sql'

для создания базовых данных воспользуйтесь кодом из файла
'MsSQL_Tables_Values.txt'

### Используемые технологии ###
Backend:
ASP.NET MVC 5
.Net frameworl 4.7.1

БД:
MSSQL 2019
NHibernate 4

Логирование:
NLog

frontend:
css,html,js,
bootstrap 4

### Краткое описание модулей ###
ELMA - главный модуль, отвечающий за запуск сайта
ELMA.DATA - модуль, в котором описаны все основные классы с параметрами для работы с данными
IELMA - модуль, содержащий в себе все интерфейсы системы
mdb - модуль для работы с БД
Mock - модуль для работы с локальными данными

### Немного о данных при реализации БД ###
При создании базовых данных для таблицы Candidates возникла трудность при записи в ручную изображения, поэтому
данные отсутствуют